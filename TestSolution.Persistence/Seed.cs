﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestSolution.Domain;

namespace TestSolution.Persistence
{
    public class Seed
    {
        public static void SeedData(DataContext dataContext)
        {
            if (!dataContext.Problems.Any())
            {
                var problems = new List<Problem>
                {
                    new Problem
                    {
                        FirstOperand = 3,
                        SecondOperand = 8
                    },
                    new Problem
                    {
                        FirstOperand = 0,
                        SecondOperand = 5
                    },
                    new Problem
                    {
                        FirstOperand = -2,
                        SecondOperand = 1
                    }
                };
                dataContext.Problems.AddRange(problems);
                dataContext.SaveChanges();
            }
        }
    }
}
