﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TestSolution.Domain;

namespace TestSolution.Persistence
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions options): base(options)
        {

        }

        public DbSet<Problem> Problems { get; set; }
        public DbSet<Book> Books { get; set; }
    }
}
