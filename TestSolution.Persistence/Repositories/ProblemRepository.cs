﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TestSolution.Domain;

namespace TestSolution.Persistence.Repositories
{
    public class ProblemRepository : IRepository<Problem>
    {
        private readonly DataContext _dataContext;

        public ProblemRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public void Create(Problem item)
        {
            _dataContext.Problems.Add(item);
        }

        public void Delete(int id)
        {
            var problem = _dataContext.Problems.Find(id);
            if (problem != null)
                _dataContext.Problems.Remove(problem);
        }


        public Problem GetEntity(int id)
        {
            return _dataContext.Problems.Find(id);
        }

        public IEnumerable<Problem> GetEntityList()
        {
            return _dataContext.Problems;
        }

        public void Save()
        {
            _dataContext.SaveChanges();
        }

        public void Update(Problem item)
        {
            _dataContext.Entry(item).State = EntityState.Modified;
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dataContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
