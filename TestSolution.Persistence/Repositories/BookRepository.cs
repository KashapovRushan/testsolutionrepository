﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TestSolution.Domain;

namespace TestSolution.Persistence.Repositories
{
    public class BookRepository : IRepository<Book>
    {   
        private readonly DataContext _dataContext;

        public BookRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public void Create(Book item)
        {
            _dataContext.Books.Add(item);
        }

        public void Delete(int id)
        {
            var book = _dataContext.Books.Find(id);
            if (book != null)
                _dataContext.Books.Remove(book);
        }

        public Book GetEntity(int id)
        {
            var result = _dataContext.Books.Find(id);
            return result;
        }

        public IEnumerable<Book> GetEntityList()
        {
            return _dataContext.Books;
        }

        public void Save()
        {
            _dataContext.SaveChanges(); 
        }

        public void Update(Book item)
        {
            _dataContext.Entry(item).State = EntityState.Modified;
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dataContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
