﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestSolution.Persistence.Repositories
{
    public interface IRepository<T> : IDisposable
        where T : class
    {
        IEnumerable<T> GetEntityList(); 
        T GetEntity(int id); 
        void Create(T item); 
        void Update(T item); 
        void Delete(int id); 
        void Save();  
    }
}
