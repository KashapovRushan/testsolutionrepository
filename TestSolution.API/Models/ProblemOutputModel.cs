﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestSolution.API.Models
{
    public class ProblemOutputModel
    {
        public int ProblemResult { get; set; }
    }
}
