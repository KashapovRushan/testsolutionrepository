﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestSolution.Domain;
using TestSolution.Services;

namespace TestSolution.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookService _bookService;

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

        [HttpGet("List/")]
        public async Task<IActionResult> List()
        {
            var result = await _bookService.GetBooks();

            return Ok(result);
        }

        [HttpGet("Details/{id}")]
        public async Task<IActionResult> Details(int id)
        {
            var result = await _bookService.GetBook(id);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post(Book book)
        {
            await _bookService.CreateBook(book);
            return CreatedAtAction(nameof(Details), new { id = book.BookId}, book);
        }

    }
}
