﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TestSolution.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoqController : ControllerBase
    {
        public async Task<IActionResult> Test()
        {
            return Ok("Hello");
        }
    }
}
