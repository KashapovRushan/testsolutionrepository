﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestSolution.API.Models;
using TestSolution.Persistence;
using TestSolution.Services;

namespace TestSolution.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class CalculatorController : ControllerBase
    {
        private readonly ICalculatorService _calculatorService;
        public CalculatorController(DataContext dataContext, ICalculatorService calculatorService)
        {
            _calculatorService = calculatorService;
        }

        [HttpGet]
        [Route("Sum/{id}")]
        public async Task<IActionResult> Sum(int id)
        {            
            var result = await _calculatorService.Sum(id);

            return Ok(new ProblemOutputModel { ProblemResult = result });
        }

        [HttpGet("Multiply/{id}")]
        public async Task<IActionResult> Multiply(int id)
        {
            var result = await _calculatorService.Multiply(id);

            return Ok(new ProblemOutputModel { ProblemResult = result });
        }
        [HttpGet("/Test")]
        public async Task<IActionResult> Test()
        {
            return Ok("Hello");
        }
    }
}
