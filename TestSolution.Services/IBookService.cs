﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestSolution.Domain;

namespace TestSolution.Services
{
    public interface IBookService
    {
        Task<Book> GetBook(int id);
        Task<List<Book>> GetBooks();
        Task CreateBook(Book book);
        Task EditBook(Book editedBook);
        Task DeleteBook(int id);
    }
}
