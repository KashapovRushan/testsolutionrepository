﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestSolution.Domain;
using TestSolution.Persistence;
using TestSolution.Persistence.Repositories;

namespace TestSolution.Services
{
    public class BookService: IBookService
    {
        private readonly IRepository<Book> _bookRepository;

        public BookService(IRepository<Book> bookRepository)
        {
            _bookRepository = bookRepository;
        }

        public async Task<Book> GetBook(int id)
        {
            var book = _bookRepository.GetEntity(id);

            if (book == null)
            {
                //
            }

            return book;
        }

        public async Task<List<Book>> GetBooks()
        {
            var books =  _bookRepository.GetEntityList().ToList();
            return books;
        }

        public async Task CreateBook(Book book)
        {
            _bookRepository.Create(book);
            _bookRepository.Save();
        }

        public async Task EditBook(Book editedBook)
        {
            _bookRepository.Update(editedBook);
            _bookRepository.Save();
        }

        public async Task DeleteBook(int id)
        {
            _bookRepository.Delete(id);
            _bookRepository.Save();
        }
    }
}
