﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestSolution.Domain;
using TestSolution.Persistence;
using TestSolution.Persistence.Repositories;

namespace TestSolution.Services
{
    public class CalculatorService: ICalculatorService
    {
        private readonly IRepository<Problem> _problemRepository;
        public CalculatorService(IRepository<Problem> problemRepository)
        {
            _problemRepository = problemRepository;
        }

        public async Task<int> Sum(int id)
        {
            var problem = _problemRepository.GetEntity(id);
            var result = problem.FirstOperand + problem.SecondOperand;
            return result;
        }

        public async Task<int> Multiply(int id)
        {
            var problem = _problemRepository.GetEntity(id);
            var result = problem.FirstOperand * problem.SecondOperand;
            return result;
        }
    }
}
