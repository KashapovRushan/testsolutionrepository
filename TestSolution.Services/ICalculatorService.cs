﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestSolution.Services
{
    public interface ICalculatorService
    {
        Task<int> Sum(int id);
        Task<int> Multiply(int id);
    }
}
