﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestSolution.Services
{
    public class GreetingsService
    {
        public string SayHi(string name)
        {
            var result = $"Hello, {name}";
            return result;
        }
    }
}
