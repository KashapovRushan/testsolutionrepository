﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentAssertions;
using Moq;
using TestSolution.Domain;
using TestSolution.Persistence;
using TestSolution.Persistence.Repositories;
using TestSolution.Services;
using Xunit;

namespace TestSolution.Tests
{
    public class CalculatorServiceTests
    {
        private readonly CalculatorService _service;
        private readonly Mock<IRepository<Problem>> _repositoryMock = new Mock<IRepository<Problem>>();

        public CalculatorServiceTests()
        {
            _service = new CalculatorService(_repositoryMock.Object);
        }

        [Theory]
        [InlineData(0, 1, 1)]
        [InlineData(1, 2, 3)]
        [InlineData(-1, 1, 0)]
        public void Sum_ShouldReturnExpectedValue(int a, int b, int c)
        {
            var id = 1;
            var problem = new Problem { FirstOperand = a, SecondOperand = b };
            _repositoryMock.Setup(x => x.GetEntity(id)).Returns(problem);

            var expected = c;

            var result = _service.Sum(id).Result;

            result.Should().Be(expected);
        }

        [Theory]
        [InlineData(1, 2, 2)]
        [InlineData(3, 4, 12)]
        [InlineData(0, 1, 0)]
        public void Multiply_ShouldReturnExpectedValue(int a, int b, int c)
        {
            var id = 1;
            var problem = new Problem { FirstOperand = a, SecondOperand = b };
            var context = _repositoryMock.Setup(x => x.GetEntity(id)).Returns(problem);
            var expected = c;

            var result = _service.Multiply(id).Result;

            result.Should().Be(expected);
        }
    }
}
