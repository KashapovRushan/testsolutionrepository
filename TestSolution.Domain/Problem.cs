﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestSolution.Domain
{
    public class Problem
    {
        public int Id { get; set; }
        public int FirstOperand { get; set; }
        public int SecondOperand { get; set; }
    }
}
