﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using TestSolution.API;
using TestSolution.Domain;
using TestSolution.IntegrationTests.Utilities;
using TestSolution.Persistence;
using TestSolution.Persistence.Repositories;
using Xunit;

namespace TestSolution.IntegrationTests
{
    public class BookControllerTests: IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;
        private readonly HttpClient _httpClient;
        
        public BookControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            factory.ClientOptions.BaseAddress = new Uri("https://localhost:5001/api/book/");
            _factory = factory;
            _httpClient = _factory.CreateClient();
            

        }

        [Theory]
        [InlineData("List/")]
        [InlineData("Details/1")]
        public async Task Get_EndpointsShouldReturnSuccessAndCorrectContentType(string url)
        {
            var response = await _httpClient.GetAsync(url);

            response.EnsureSuccessStatusCode();
            response.Content.Headers.ContentType.MediaType.Should().Be("application/json");
            
        }

        [Fact]
        public async Task List_ShouldReturnExpectedValue()
        {
            var expected = TestDbInitializer.SeedWithBooks();

            var result = await _httpClient.GetFromJsonAsync<List<Book>>("List/");

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public async Task Details_ShouldReturnExpectedValue()
        {
            var expected = TestDbInitializer.SeedWithBooks().First();

            var result = await _httpClient.GetFromJsonAsync<Book>($"Details/{expected.BookId}");

            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public async Task Post_WithValidBookShouldReturnCreatedResult()
        {
            var book = new Book { Name = "Valid book name", Author = "Valid book author" };
            var content = JsonContent.Create(book);

            var response = await _httpClient.PostAsync("", content);

            response.StatusCode.Should().Be(HttpStatusCode.Created);

        }

        [Fact]
        public async Task Post_WithoutNameReturnBadRequest()
        {
            var book = new Book { Name = null, Author = "Valid book author" };
            var content = JsonContent.Create(book);

            var response = await _httpClient.PostAsync("", content);

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

        }

    }
}
