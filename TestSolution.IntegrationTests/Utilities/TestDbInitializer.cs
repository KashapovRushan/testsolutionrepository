﻿using System;
using System.Collections.Generic;
using System.Text;
using TestSolution.Domain;
using TestSolution.Persistence;

namespace TestSolution.IntegrationTests.Utilities
{
    public class TestDbInitializer
    {
        public static void InitializeDbForTests(DataContext dataContext)
        {
            dataContext.AddRange(SeedWithProblems());
            dataContext.AddRange(SeedWithBooks());
            dataContext.SaveChanges();
        }

        public static void ReinitializeDbForTests(DataContext dataContext)
        {
            dataContext.Problems.RemoveRange(dataContext.Problems);
            dataContext.Books.RemoveRange(dataContext.Books);
            InitializeDbForTests(dataContext);
        }

        public static List<Problem> SeedWithProblems()
        {
            return new List<Problem>()
            {
                new Problem{FirstOperand=1,SecondOperand=2},
                new Problem{FirstOperand=2,SecondOperand=1},
                new Problem{FirstOperand=0,SecondOperand=2},
                new Problem{FirstOperand=1,SecondOperand=0},
                new Problem{FirstOperand=-1,SecondOperand=2},
                new Problem{FirstOperand=1,SecondOperand=-2}
            };
        }

        public static List<Book> SeedWithBooks()
        {
            return new List<Book>()
            {
                new Book{BookId = 1, Name="Test Book 1", Author="Test Author 1"},
                new Book{BookId = 2, Name="Test Book 2", Author="Test Author 2"},
                new Book{BookId = 3, Name="Test Book 3", Author="Test Author 3"},
                new Book{BookId = 4, Name="Test Book 4", Author="Test Author 4"},
            };
        }
    }
}
