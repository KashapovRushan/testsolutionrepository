﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestSolution.IntegrationTests.Models
{
    public class ExpectedProblemOutputModel
    {
        public int ProblemResult { get; set; }
    }
}
