﻿using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using TestSolution.API;
using Xunit;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using TestSolution.Persistence;
using Microsoft.EntityFrameworkCore;
using TestSolution.IntegrationTests.Models;

namespace TestSolution.IntegrationTests
{
    public class CalculatorControllerTests: IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _httpClient;
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public CalculatorControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            factory.ClientOptions.BaseAddress = new Uri("https://localhost:5001/api/calculator/");
            _factory = factory;
            _httpClient = _factory.CreateClient();
        }

        [Theory]
        [InlineData(1, 3)]
        [InlineData(2, 3)]
        [InlineData(3, 2)]
        [InlineData(4, 1)]
        [InlineData(5, 1)]
        [InlineData(6, -1)]
        public async Task Sum_ShouldReturnExpectedValue(int problemId, int expected)
        {
            var response = await _httpClient.GetFromJsonAsync<ExpectedProblemOutputModel>($"Sum/{problemId}");

            response.Should().NotBeNull();
            response.ProblemResult.Should().Be(expected);
        }

        [Theory]
        [InlineData(1, 2)]
        [InlineData(2, 2)]
        [InlineData(3, 0)]
        [InlineData(4, 0)]
        [InlineData(5, -2)]
        [InlineData(6, -2)]
        public async Task Multiply_ShouldReturnExpectedValue(int problemId, int expected)
        {
            var response = await _httpClient.GetFromJsonAsync<ExpectedProblemOutputModel>($"Multiply/{problemId}");

            response.Should().NotBeNull();
            response.ProblemResult.Should().Be(expected);
        }

        [Theory]
        [InlineData("Sum/1")]
        [InlineData("Multiply/1")]
        public async Task Get_EndpointShouldReturnSuccessAndCorrectContentType(string url)
        {
            var response = await _httpClient.GetAsync(url);

            response.EnsureSuccessStatusCode();
            response.Content.Headers.ContentType.MediaType.Should().Be("application/json");
        }
    }
}
