﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestSolution.Domain;
using TestSolution.Persistence;
using TestSolution.Persistence.Repositories;
using TestSolution.Services;

namespace TestSolution.IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(
                d => d.ServiceType ==
                    typeof(DbContextOptions<DataContext>));

                services.Remove(descriptor);

                services.AddDbContext<DataContext>(options =>
                {
                    options.UseInMemoryDatabase(databaseName: "testDb");
                });

                services.AddTransient<ICalculatorService, CalculatorService>();
                services.AddTransient<IBookService, BookService>();
                services.AddTransient<IRepository<Problem>, ProblemRepository>();
                services.AddTransient<IRepository<Book>, BookRepository>();
                
                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                {
                    DataContext dataContext = scope.ServiceProvider.GetRequiredService<DataContext>();
                    ILogger logger = scope.ServiceProvider.GetRequiredService<ILogger<WebApplicationFactory<TStartup>>>();

                    dataContext.Database.EnsureCreated();

                    try
                    {
                        Utilities.TestDbInitializer.ReinitializeDbForTests(dataContext);

                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, "Error while initializing test database. Error message: " + ex.Message);
                    }
                }

            });



            
        }
    }
}
